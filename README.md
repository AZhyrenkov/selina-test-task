# Selina home assignment
This repository consist of all source scripts used for home assignment for BI Developer position.

## Setting the dev env
Local instance of postgres is set over docker-compose with persistant storage:
```
docker-compose up
```
---
## Main data model principles and ideas
1. The amount of data is not significant, so there was a decision to make all fact tables as views. However, in production environments it would be iterative day-to-day load.
1. The database engine chosen was `PostgreSQL:latest` because it's the closest analogue for AWS Redshift used by Selina for DWH purposes
1. The DWH concept was chosen as a star-model with fact and dimensions tables.
1. Source tables are kept in _raw_ way as in data source, all transformations are done in separate tables/views.
1. All tables\views are loaded into Tableau for dataviz, one custom query for a cohort analysis is located at `sql/91_cohorts_analysis.sql`
1. Naming of sql-files with 2-digit number prefixes for the sake of right order (inspired by migrations concept in web-dev)

### Naming - Tables and views
The main principle of naming all tables/views in db is `{table_type}{date part}{meaning}`.

**Table type**
- `src` - _data source_ exact data structure, no additional columns, but proper types. 
- `anlt` - _analytical_ - those type of tables/views are adding additional columns, data transformation etc.
- `dim` - _dimension_ tables.
- `fact` - _fact_ tables with calculated kpis for respective _date part_.

**Date part** means analugie of _date partition_. So far used:
- `daily`
- `weekly`

### Naming - Abbreviations
- `nk` - natural key
- `agg` - aggregated data (usually on date level, meaning no user_id in table)

---

### List of tables created:
- `src_wifi_log` - source table as it is.
- `anlt_wifi_log` - source table with start/end session time as timestamps.
- `dim_users` - list of all users with their first connection date as attribute.
- `dim_dates` - dates dimension, calendar with all needed date formats prepared.
- `fact_houry_audience` - number of sessions by user_id day-hour-wised.
- `fact_daily_audience` - number of sessions by user_id day-wised.
- `fact_weeky_audience` - number of sessions by user_id week-wised.
- `fact_agg_daily_audience` - number of sessions, avg session duration and unique users day-wised.
- `fact_agg_weekly_audience` - number of sessions, avg session duration and unique users week-wised.

### List of KPIs monitored:
- _sessions_ count.
- _unique users_ count.
- _average session_ duration 

---

## Tableau links
- [Selina - General WiFi Dashboard](https://public.tableau.com/app/profile/oleksii.zhyrenkov/viz/Selina-GeneralWiFiDashboard/Generalstat)
- [Selina - Cohorts analysis](https://public.tableau.com/app/profile/oleksii.zhyrenkov/viz/Selina-Cohortanalysis/Cohortanalysisofcheck-indateanddayofstay)
- [Selina - Hourly load](https://public.tableau.com/app/profile/oleksii.zhyrenkov/viz/Selina-Hourlyload/Hourlypayload)

---
## Assumptions and tricks
1. First connection date can be treated as check-in date. Of course in productions environments this cohort reference could be pointed by actual data source (e.g. booking system microservice).
1. Days after first connection date are treated as _Day of stay_, just to bring the look of real data for this test task.
1. The data consist of two full weeks and 2 partial thus on weekly level there may be some misaundersandings, but this is a test task, isn't it? :)
1. Of course average of average is not the best metric, in case of business need for a better session duration analysis, I would go for other metrics and techniques, like medians, modes and distribution analysis.
1. The source csv file was loaded into personal aws account to show up practical usage of AWS and boto3 .
1. For debugging purposes the _notebook.ipynb_ was used with moving the code into separate `.py` files for productions scripts.
1. After basic explorative analysis it looks like average number of daily sessions per user is ~1, so for the sake of simplicity major part of KPIs are on session level.