def execute_queries(engine, filepath: str):
    with open(filepath, 'r') as file:
        queries = file.read().split(';')
        for q in queries:
            if len(q) > 0:
                print('Executing:\n{}'.format(q))
                engine.execute(q)