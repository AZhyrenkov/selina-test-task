WITH src AS (
    SELECT u.user_id
         , u.first_connection_date
         , d.date_nk
    FROM dim_dates d
    CROSS JOIN dim_users u
    WHERE d.date_nk >= u.first_connection_date
    ORDER BY u.user_id, d.date_nk
)
, anlt AS (
    SELECT src.user_id
         , src.first_connection_date as cohort
         , src.date_nk
         , coalesce(a.sessions, 0)      as sessions
         , lead(coalesce(a.sessions, 0), 1) over (PARTITION BY src.user_id ORDER BY src.date_nk) as sessions_lead_1d
         , lead(coalesce(a.sessions, 0), 2) over (PARTITION BY src.user_id ORDER BY src.date_nk) as sessions_lead_2d
         , lead(coalesce(a.sessions, 0), 3) over (PARTITION BY src.user_id ORDER BY src.date_nk) as sessions_lead_3d
         , row_number() over (PARTITION BY src.user_id ORDER BY src.date_nk)  as user_day_age
    FROM src
    LEFT JOIN fact_daily_audience a ON a.user_id = src.user_id AND a.date_nk = src.date_nk
    ORDER BY src.user_id, src.date_nk
)
SELECT
       cohort
     , user_day_age
     , count(DISTINCT user_id)              as users_cohort
     , count(DISTINCT user_id)
         FILTER ( WHERE sessions > 0 )            as unique_users
     , count(DISTINCT user_id)
         FILTER ( WHERE sessions_lead_1d > 0 ) as users_1d
     , count(DISTINCT user_id)
         FILTER ( WHERE sessions_lead_1d > 0
                    AND sessions_lead_2d > 0) as users_2d
     , count(DISTINCT user_id)
         FILTER ( WHERE sessions_lead_1d > 0
                    AND sessions_lead_2d > 0
                    aND sessions_lead_3d > 0) as users_3d
FROM anlt
-- WHERE cohort = '2017-11-05'
GROUP BY 1,2;