SET search_path = public;

CREATE OR REPLACE VIEW anlt_wifi_log AS
    SELECT *
         , to_timestamp(connection_date || ' ' || start_hour || ':' || start_minute,
                        'YYYYMMDD HH24:MI')                                                          as start_timestamp
         , to_timestamp(connection_date || ' ' || end_hour || ':' || end_minute, 'YYYYMMDD HH24:MI') as end_timestamp
    FROM src_wifi_log;