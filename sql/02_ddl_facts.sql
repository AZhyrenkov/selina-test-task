CREATE OR REPLACE VIEW fact_daily_audience AS
SELECT
    d.date_nk
    ,l.user_id
    ,count(1)                                                   as sessions
    ,extract(EPOCH FROM avg(end_timestamp - start_timestamp))   as avg_session_duration
FROM anlt_wifi_log l
JOIN dim_dates d ON d.date_int = l.connection_date
GROUP BY 1,2
ORDER BY 1,2;

CREATE OR REPLACE VIEW fact_weekly_audience AS
SELECT
    d.week_start_date
    ,l.user_id
    ,count(1)                                                   as sessions
    ,extract(EPOCH FROM avg(end_timestamp - start_timestamp))   as avg_session_duration
FROM anlt_wifi_log l
JOIN dim_dates d ON d.date_int = l.connection_date
GROUP BY 1,2
ORDER BY 1,2;

CREATE OR REPLACE VIEW fact_agg_daily_audience AS
SELECT
    d.date_nk
    ,count(DISTINCT user_id)                                    as unique_users
    ,count(1)                                                   as sessions
    ,extract(EPOCH FROM avg(end_timestamp - start_timestamp))   as avg_session_duration
FROM anlt_wifi_log l
JOIN dim_dates d ON d.date_int = l.connection_date
GROUP BY 1
ORDER BY 1;

CREATE OR REPLACE VIEW fact_agg_weekly_audience AS
SELECT
    d.week_start_date
    ,count(DISTINCT user_id)                                    as unique_users
    ,count(1)                                                   as sessions
    ,extract(EPOCH FROM avg(end_timestamp - start_timestamp))   as avg_session_duration
FROM anlt_wifi_log l
JOIN dim_dates d ON d.date_int = l.connection_date
GROUP BY 1
ORDER BY 1;

CREATE OR REPLACE VIEW fact_hourly_audience AS
   SELECT
    d.date_nk
    , d.weekday
    , start_hour as hour
    , count(1)  as sessions
    , count(DISTINCT a.user_id) as unique_users
FROM anlt_wifi_log a
JOIN dim_dates d ON d.date_int = a.connection_date
GROUP BY 1,2,3
ORDER BY 1,2,3;